const userService = {
  players: [
    {
      name: 'Frozen',
      surname: 'Yogurt',
      age: 24
    },
    {
      name: 'Ice',
      surname: 'Cream',
      age: 25
    }
  ],
  Index: -1,
  addItem () {
    console.log('Add')
    if (this.Index > -1) {
      Object.assign(this.players[this.Index], this.editedItem)
    } else {
      this.players.push(this.editedItem)
    }
    this.close()
  },
  editItem (data) {
    console.log('Edit')
    this.dialog = true
    this.Index = this.players.indexOf(data.item)
    this.editedItem = Object.assign({}, data.item)
  },
  deleteItem (data) {
    console.log('Delete')
    this.dialogDelete = true
    this.Index = this.players.indexOf(data.item)
    this.editedItem = Object.assign({}, data.item)
  }
}
export default userService
